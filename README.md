# Flutter Library that works

## [JSON Serialization](https://flutter.dev/docs/development/data-and-backend/json)
```yaml
json_serializable: ^2.0.0
json_annotation: ^2.0.0
```
Particularly important for working with API.

## [Google Map](https://pub.dartlang.org/packages/google_maps_flutter)
```yaml
google_maps_flutter: ^0.0.3+3
```

## [Get Current Location](https://pub.dartlang.org/packages/location)
```yaml
location: ^1.4.1
```

## [Redirect to Google Map/Contact](https://pub.dartlang.org/packages/url_launcher)
```yaml
url_launcher: ^4.0.3
```

## [Carousel](https://github.com/jlouage/flutter-carousel-pro)
```yaml
carousel_pro: ^0.0.13
```

## [Device Local Storage](https://pub.dartlang.org/packages/shared_preferences)
```yaml
shared_preferences: ^0.4.3
```
  
## [Load SVG Image](https://pub.dartlang.org/packages/flutter_svg)
```yaml
flutter_svg: ^0.10.2
```
## Social Login
#### [Google](https://pub.dartlang.org/packages/google_sign_in)
```yaml
google_sign_in: ^4.0.0
```
#### [Facebook](https://pub.dartlang.org/packages/flutter_facebook_login)
```yaml
flutter_facebook_login: ^1.2.0
```

## [Display Date Format](https://pub.dartlang.org/packages/intl)
```yaml
intl: ^0.15.7
```

## [Check Internet Connection](https://pub.dartlang.org/packages/connectivity)
```yaml
connectivity: ^0.4.0
```
## [Fontawesome](https://pub.dartlang.org/packages/font_awesome_flutter)
```yaml
font_awesome_flutter: ^8.2.0
```

## [Camera and Gallery](https://pub.dartlang.org/packages/image_picker)
```yaml
image_picker: ^0.4.10 # Must use this version if latest doesn't work
```

## [Case String Format](https://pub.dartlang.org/packages/recase)
```yaml
recase: ^2.0.1
```

## [Custom Fonticon](http://fluttericon.com)
Visit [fluttericon.com](http://fluttericon.com)

## [Local Database SQLite](https://pub.dartlang.org/packages/sqflite)
```yaml
sqflite: ^1.1.0
```

## [Localization](https://pub.dartlang.org/packages/intl_translation)
```yaml
intl_translation: ^0.17.1
```

## Function for uploading image
```dart
static Future<JustResponse> storeMedia() async {
  File image = ...;
  print("size is ${image.lengthSync()}");
  ByteStream stream =
      http.ByteStream(DelegatingStream.typed(image.openRead()));
  int length = await image.length();

  Uri uri = Uri.parse(
    "upload_route",
  );

  MultipartRequest request = http.MultipartRequest("POST", uri);
  MultipartFile multipartFile = http.MultipartFile(
    "photo", // Field name
    stream,
    length,
    filename: basename(image.path),
  );

  request.files.add(multipartFile);
  request.fields.addEntries(
    [
      MapEntry(
        "form_field",
        [Form Value],
      ),
    ],
  );
  var response = await request.send();
  print(response.statusCode);

  await for (var tick in response.stream.transform(utf8.decoder)) {
    print("Response is "+tick);
    return JustResponse.fromJson(json.decode(tick));
  }
  return null;
}
```